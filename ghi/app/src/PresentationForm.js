import React, { useEffect, useState } from 'react';


function PresentationForm () {
    const[conferences, setConferences] = useState([]);
    const[formData, setFormData] = useState({
        name: '',
        presenterName: '',
        presenterEmail: '',
        companyName: '',
        title: '',
        synopsis: '',
        conference: '',
    });

    const fetchData = async () => {
        const url = "http://localhost:8000/api/conferences/";
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setConferences(data.conferences)
        }
    }

    useEffect(() => {
        fetchData ();
    }, []);

    const handleFormDataChange = async (event) => {
        const name = event.target.name;
        const value = event.target.value;
        setFormData({...formData, [name]: value});
    };

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};

        data.presenter_name = formData.presenterName;
        data.presenter_email = formData.presenterEmail;
        data.company_name = formData.companyName;
        data.title = formData.title;
        data.synopsis = formData.synopsis;
        data.conference = formData.conference;

        const presentationUrl = `http://localhost:8000${data.conference}presentations/`;
		const fetchConfig = {
			method: "post",
			body: JSON.stringify(data),
			headers: {
				"Content-Type": "application/json",
			},
		};

		const response = await fetch(presentationUrl, fetchConfig);
		if (response.ok) {
			setFormData({
                presenterName: '',
                presenterEmail: '',
                companyName: '',
                title: '',
                synopsis: '',
                conference: '',
            });
		}
    }

    return (
    <div className="container">
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new presentation</h1>
            <form onSubmit={handleSubmit} id="create-presentation-form">
              <div className="form-floating mb-3">
                <input onChange={handleFormDataChange} value={formData.presenterName} placeholder="Presenter name" required type="text" name="presenterName" id="presenterName" className="form-control" />
                <label htmlFor="presenter_name">Presenter name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleFormDataChange} value={formData.presenterEmail} placeholder="Presenter email" required type="email" name="presenterEmail" id="presenterEmail" className="form-control" />
                <label htmlFor="presenter_email">Presenter email</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleFormDataChange} value={formData.companyName} placeholder="Company name" type="text" name="companyName" id="companyName" className="form-control" />
                <label htmlFor="company_name">Company name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleFormDataChange} value={formData.title} placeholder="Title" required type="text" name="title" id="title" className="form-control" />
                <label htmlFor="title">Title</label>
              </div>
              <div className="form mb-3">
                <label htmlFor="synopsis">Synopsis</label>
                <textarea onChange={handleFormDataChange} value={formData.synopsis} id="synopsis" name="synopsis" className="form-control" rows="6" cols="30"></textarea>
              </div>
              <div className="mb-3">
                <select onChange={handleFormDataChange} value={formData.conference} required id="conference" name="conference" className="form-select">
                  <option value="">Choose a conference</option>
                  {conferences.map(conference => {
                    return (
                        <option key={conference.href} value={conference.href}>{conference.name}</option>
                    );
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    </div>
    )
}

export default PresentationForm;
